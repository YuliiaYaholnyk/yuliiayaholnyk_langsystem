
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';

import 'dictionary_classes/language.dart';
import 'dictionary_data/en.dart';
import 'dictionary_data/ru.dart';
class Dictionary {
  Locale locale;
 Language language;
  static Dictionary instance;
  Dictionary(this.locale) {
    // set up of default language
      language = en;
  }
  static Dictionary of(BuildContext context) {
    return Localizations.of<Dictionary>(context, Dictionary);
  }
  static void init(BuildContext context) {
    instance = Dictionary.of(context);
  }
  //region [RTL LANGUAGES]
  static const List<String> _rtlLanguages = <String>[
    'ar', // Arabic
    'fa', // Farsi
    'he', // Hebrew
    'ps', // Pashto
    'ur', // Urdu
  ];
  bool isRTL(BuildContext context) {
    return _rtlLanguages.contains(
      Localizations.localeOf(context).languageCode,
    );
  }
//endregion
}
class FlutterDictionaryDelegate extends LocalizationsDelegate<Dictionary> {
  const FlutterDictionaryDelegate();
  ///locales added here are supported by the dictionary, but not yet by the app
  @override
  bool isSupported(Locale locale) {
    return ['en', 'ru'].contains(locale.languageCode);
  }
  @override
  Future<Dictionary> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<Dictionary>(Dictionary(locale));
  }
  @override
  bool shouldReload(LocalizationsDelegate<Dictionary> old) {
    return false;
  }
}