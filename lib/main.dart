import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:languageApp/providers/lang_provider.dart';
import 'package:provider/provider.dart';
import 'dictionary/dictionary.dart';
import 'dictionary/dictionary_data/en.dart';
import 'dictionary/dictionary_data/ru.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LangProvider>(
      create: (_) => LangProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.yellow,
        ),
        home: MyHomePage(),
        onGenerateTitle: (context) {
          Dictionary.init(context);
          return Dictionary.instance.language.mainPageLanguage.title;
        },
        localizationsDelegates: [
          const FlutterDictionaryDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    //final langProviderData = Provider.of<LangProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(Dictionary.instance.language.mainPageLanguage.title),
        leading: IconButton(
          icon: Icon(Icons.language),
          onPressed: () {
            setState(
              () {
                Dictionary.instance.language = ru;
              },
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.black12,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(border: Border.all(color: Colors.deepOrange, width: 2.0,style: BorderStyle.solid)),
                height: 200,
                margin: EdgeInsets.only(top: 10.0),
                child: Image(
                  image: NetworkImage('https://cdn.pixabay.com/photo/2020/02/12/05/17/dog-cartoon-4841705_960_720.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              Container(padding: EdgeInsets.all(10), child: Text(Dictionary.instance.language.mainPageLanguage.description)),
            ],
          ),
        ),
      ),
    );
  }
}
